import express from "express";
import { logIn, removeArticle, showAllArticles, showByAuthor, showByTitle, signUp } from "../middleware/user.middleware.js";

const router = express.Router();



/**
 * @swagger
 * /signUp:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Signup
 *    description: Register a user in the database
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Password
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: User Created
 *            404:
 *                description: User already exists
 * 
 */


router.post("/signUp", (req, res) => {
    signUp(req, res);
});

/**
 * @swagger
 * /logIn:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Login
 *    description: Login a user and return an token
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Password
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Correct user
 *            404:
 *                description: Wrong password or username
 * 
 */


router.post("/logIn", (req, res) => {
    logIn(req, res);
});

/**
 * @swagger
 * /showArticles/{page}:
 *  get:
 *    tags: 
 *      [Articles]
 *    summary:  Show all articles
 *    description:  Show all articles saved in the database in rounds of 5
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: page
 *      description: Current page the user is on
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *            200:
 *                description: Usuario deslogeado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 */

router.get("/showArticles/:page", (req, res) => {
    showAllArticles(req, res);
});



/**
* @swagger
* /showArticles/author/{author}/{page}:
*  get:
*    tags: 
*      [Articles]
*    summary:  Show articles by one author
*    description:  Show all articles saved of that author in the database in rounds of 5
*    parameters:
*    - name: authorization
*      description: User Token
*      in: header
*      required: true
*      type: string
*    - name: page
*      description: Current page the user is on
*      in: path
*      required: true
*      type: integer
*    - name: author
*      description: Name of the author of the article/s
*      in: path
*      required: true
*      type: string
*    responses:
*            200:
*                description: Usuario deslogeado exitosamente
*            403:
*                description: Credenciales incorrectas
*            400:
*                description: Error al validar los datos 
*/

router.get("/showArticles/author/:author/:page", (req, res) => {
    showByAuthor(req, res);
});

/**
* @swagger
* /showArticles/title/{title}:
*  get:
*    tags: 
*      [Articles]
*    summary:  Show article by title
*    description:  Show the article that matches the one provided
*    parameters:
*    - name: authorization
*      description: User Token
*      in: header
*      required: true
*      type: string
*    - name: title
*      description: Title of the article
*      in: path
*      required: true
*      type: string
*    responses:
*            200:
*                description: Usuario deslogeado exitosamente
*            403:
*                description: Credenciales incorrectas
*            400:
*                description: Error al validar los datos 
*/

router.get("/showArticles/title/:title", (req, res) => {
    showByTitle(req, res);
});

/**
* @swagger
* /showArticles/remove/{id}:
*  delete:
*    tags: 
*      [Articles]
*    summary:  Block article
*    description:  Block an article from being displayed again
*    parameters:
*    - name: authorization
*      description: User Token
*      in: header
*      required: true
*      type: string
*    - name: id
*      description: Id of the article to block
*      in: path
*      required: true
*      type: string
*    responses:
*            200:
*                description: Usuario deslogeado exitosamente
*            403:
*                description: Credenciales incorrectas
*            400:
*                description: Error al validar los datos 
*/

router.delete("/showArticles/remove/:id", (req, res) => {
    removeArticle(req, res);
});

export { router };
