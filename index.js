import "dotenv/config.js";
import express from "express";
import { getArticles } from "./controllers/article.controller.js";
import { showAllArticles } from "./middleware/user.middleware.js"
import swaggerUI from "swagger-ui-express"
import swaggerJsDoc from "swagger-jsdoc"
import { router as UserRout } from "./routes/user.routes.js";



const app = express();
const port = process.env.PORT || 3000;

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Reing App',
            version: '1.0.0'
        }
    },
    apis: ['./routes/user.routes.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

app.use(UserRout)


// await getArticles()

// setInterval(getArticles,1000 * 60 * 60)


app.listen(port, () => {
    console.log(`App port: http://localhost:${port}/`);
    console.log(`Swagger: http://localhost:${port}/api-docs`);
})