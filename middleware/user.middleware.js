import { Article } from '../connection/database.config.js';
import { banArticle, consumeToken, createNewUser, loginUser, showArticles } from '../controllers/user.controller.js';


export const signUp = async (req, res) => {
    const response = await createNewUser(req.body);
    res.status(response.status).json(response.msj)
}

export const logIn = async (req, res) => {
    const response = await loginUser(req.body);
    res.status(response.status).json(response.msj)
}

export const showAllArticles = async (req, res) => {
    const userData = consumeToken(req.headers.authorization);
    if (userData != false) {
        const database = await Article.find();
        const articles = await showArticles(parseInt(req.params.page), database, userData.id);
        res.status(200).json(articles)
    } else {
        res.status(403).json({ msj: 'Invalid Token' })
    }
}

//page, author
export const showByAuthor = async (req, res) => {
    const userData = consumeToken(req.headers.authorization);
    if (userData != false) {
        const database = await Article.find({ author: req.params.author });
        const articles = await showArticles(parseInt(req.params.page), database, userData.id);
        res.status(200).json(articles)
    } else {
        res.status(403).json({ msj: 'Invalid Token' })
    }
}

export const showByTitle = async (req, res) => {
    const userData = consumeToken(req.headers.authorization);
    if (userData != false) {
        const database = await Article.findOne({ story_title: req.params.title });
        res.status(200).json(database)
    } else {
        res.status(403).json({ msj: 'Invalid Token' })
    }
}

export const removeArticle = async (req, res) => {
    const userData = consumeToken(req.headers.authorization);
    if (userData != false) {
        const response = await banArticle(userData,req.params.id);
        res.status(response.status).json(response.msj)
    } else {
        res.status(403).json({ msj: 'Invalid Token' })
    }
}