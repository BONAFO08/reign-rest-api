import "dotenv/config.js";
import mongoose from "mongoose";
import articleSchema from "../models/article.model.js";
import userSchema from "../models/user.model.js";

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

const MONGODB_HOST = process.env.MONGODB_HOST;
const MONGODB_PORT = process.env.MONGODB_PORT;
const MONGODB_MOVIE_DB_NAME = process.env.MONGODB_MOVIE_DB_NAME;

const conectionString = `mongodb://${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_MOVIE_DB_NAME}`;

mongoose.connect(conectionString, options);

const User = mongoose.model('users', userSchema);
const Article = mongoose.model('articles', articleSchema);


export {
    mongoose,
    User,
    Article
};