
 const articleSchema = {
    created_at : String,
    author : String,
    story_title : String,
    story_url : String,
    comment_text : String,
    _tags : []
};



export default articleSchema;