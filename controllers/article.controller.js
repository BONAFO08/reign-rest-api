import fetch from 'node-fetch';
import { Article } from '../connection/database.config.js';

const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

const saveArticle = async (articleData) => {
    const article = await new Article({
        created_at: articleData.created_at,
        author: articleData.author,
        story_title: (articleData.story_title != null) ? (articleData.story_title) : (articleData.title),
        story_url: articleData.story_url,
        comment_text: articleData.comment_text,
        _tags : articleData._tags,
    });
    // cache.push(article)
    const result = await article.save();
    return result
};

export const getArticles = async () => {
    const response = await fetch(url);
    const articleData = await response.json();
    
    const database = await Article.find();

    for (let i = 0; i < articleData.hits.length; i++) {
        let validator;
        (articleData.hits[i].story_title != null)
            ? (validator = database.filter(arr => arr.story_title == articleData.hits[i].story_title))
            : (validator = database.filter(arr => arr.story_title == articleData.hits[i].title));
        
        if(validator.length == 0) saveArticle(articleData.hits[i])

        }
}

export const cache = [];


getArticles();