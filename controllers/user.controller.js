import "dotenv/config.js";
import { User, Article } from '../connection/database.config.js';
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"

//Encrypt password
const cripty = async (password) => {
    const pass = await bcrypt.hash(password, 8);
    return pass;
}

//Compare password
const decripty = async (password, oldPassword) => {
    const aux = await bcrypt.compare(password, oldPassword);
    return aux;
}

//Create token
const newTokensing = async (data) => {
    const token = jwt.sign(data, process.env.JWT_SECRET, { expiresIn: 1000 * 60 * 60 * 24 });
    return token;

}


export const consumeToken = (token) => {
    try {
        let cleantoken = token;
        cleantoken = cleantoken.replace('Bearer ', '');
        cleantoken = cleantoken.replace(/"/g, '')
        const detoken = jwt.verify(cleantoken, process.env.JWT_SECRET);
        return detoken;
    } catch (error) {
        return false;
    }
}



export const showArticles = async (actualPage, arrArticles, userID) => {
    let page = actualPage * 5;
    const pageContain = [];
    const userData = await User.findOne({ _id: userID });

    do {
        let bannedArticle;
        try {
            bannedArticle = userData.bannedArticles.filter(arr => arr == arrArticles[page]._id);
        } catch (error) {
            bannedArticle = [];
        }

        if (bannedArticle.length != 0) {
            page += 1
        } else {
            pageContain.push(arrArticles[page])
            page += 1
        }

    } while (pageContain.length < 5);

    // for (let i = page; i < page + 5; i++) {
    //     const bannedArticle = userData.bannedArticles.filter(arr => arr == arrArticles[i]._id);
    //     pageContain.push(arrArticles[i])
    // }

    return pageContain;
}

export const createNewUser = async (dataUser) => {
    const searchUser = await User.findOne({ username: dataUser.username });
    if (searchUser == null) {
        const encryptedPassword = await cripty(dataUser.password);
        const user = await new User({
            username: dataUser.username,
            password: encryptedPassword,
            bannedArticles: []
        });
        const result = await user.save();
        return { msj: 'User created', status: 200 }
    } else return { msj: 'User already exists ', status: 404 }
}

export const loginUser = async (dataUser) => {
    const searchUser = await User.findOne({ username: dataUser.username });
    if (searchUser != null) {
        const comparePassword = await decripty(dataUser.password, searchUser.password);
        if (comparePassword == true) {
            const token = await newTokensing({ id: searchUser._id, username: searchUser.username })
            return { msj: token, status: 200 }
        } else return { msj: 'Wrong username or password', status: 403 }
    }
    else return { msj: 'Wrong username or password', status: 403 }
}

export const banArticle = async (dataUser, article) => {
    let database;
    try {
        database = await Article.findOne({ _id: article });
    } catch (error) {
        database = false;
    }
    if (database != false) {
        const user = await User.updateOne(
            { _id: dataUser.id },
            { $push: { bannedArticles: article } }
        )
        return { msj: 'Removed', status: 200 }
    } else return { msj: 'Article not found', status: 404 }
}
